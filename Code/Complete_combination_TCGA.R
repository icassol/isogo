#Complete_combination_TCGA


library(Matrix)
library(matrixStats)
library(glmnet)
library(PRROC)
library(ROCR)
library(glmnet)
library(arm)
library(parallel)


# LOAD TRAIN AND TEST DATA


load("./InitialData/CCLE/ExprTM.Rdata")
load("./InitialData/CCLE/ExprTT.Rdata")
load("./InitialData/GeneGOCoding.Rdata")

GeneGO <- GeneGOCoding
Expr <- rbind(ExprTM,ExprTT)

iGO <- match(rownames(Expr),rownames(GeneGO))
iGO <- iGO[!is.na(iGO)] 
GeneGO <- GeneGO[iGO,]
identical(rownames(Expr),rownames(GeneGO))

#load correlation
load("./Data_output/correlation_tcga/Z_TCGA_COMPLETE.RData") #complete model

#load domains
load("./Data_output/Domains/Pb_complete.RData") #complete model



# bayesian glm with interactions between correlation and domains methods
n <- 1
formula <- GeneGO[,n] ~ Pb_complete[,n] + Z_TCGA_COMPLETE[,n] + Pb_complete[,n] * Z_TCGA_COMPLETE[,n] + I(Pb_complete[,n]*Pb_complete[,n]) + I(Z_TCGA_COMPLETE[,n]*Z_TCGA_COMPLETE[,n])
head(model.matrix(formula))

colVar <- ncol(model.matrix(formula))
Coeficientes <- matrix(0, nrow =colVar, ncol =ncol(GeneGO))

vcovSalidasUnion <- array(data = NA, dim = c(colVar,colVar,ncol(GeneGO)))

pb <- txtProgressBar(min = 1, max =ncol(GeneGO) , style = 3)
for (n in 1:ncol(GeneGO)){
  Salida<-bayesglm(formula, family = "binomial") 
  Coeficientes[,n] <- Salida$coefficients
  vcovSalidasUnion[,,n] <- vcov(Salida)
  setTxtProgressBar(pb, n)
}

Coeficientes[2,which(is.na(Coeficientes[2,])==TRUE)]<-0

Coeficientes_Complete_TCGA <- Coeficientes
rm(Coeficientes)
gc()


save(Coeficientes_Complete_TCGA,file="./Data_output/combination/TCGA/Coeficientes_Complete_TCGA.RData")


Vcov_Complete_TCGA <- vcovSalidasUnion
rm(vcovSalidasUnion)
save(Vcov_Complete_TCGA,file="./Data_output/combination/TCGA/Vcov_Complete_TCGA.RData")




